import React from 'react';

import Icon from '@material-ui/core/Icon';

import Avatar from '@material-ui/core/Avatar';


import './Navbar.scss'

const Navbar = () => {
    return (
        <div className='navbar'>
            <div className='navbar-left-side'>
                <Icon style={{width:55, height:30}}>start</Icon>
                <h2 className='navbar-left-side-title'>Азино три топора</h2>
            </div>
            <div className="navbar-right-side">
                <span className='navbar-right-side-balance'>Balance:</span>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
            </div>
        </div>
);
};

export default Navbar;
