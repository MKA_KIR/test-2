import React from 'react';
import { DataGrid } from '@material-ui/data-grid';

import FormDialog from "../FormDialog";
import Slot from "../Slot";

const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'Slot1', headerName: 'Slot1', width: 130 },
    { field: 'Slot2', headerName: 'Slot2', width: 130 },
    { field: 'Slot3', headerName: 'Slot3', width: 130 },
    { field: 'Time', headerName: 'Time', width: 130 },

];

const rows = [
    { id: 1, Slot1: 'Slot1', time: 'Jon'},
    { id: 2, Slot2: 'Slot2', time: 'Cersei'},
    { id: 3, Slot3: 'Slot3', time: 'Jaime'},
];

export default function DataTable() {

    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid rows={rows} columns={columns} pageSize={5} checkboxSelection />
            <FormDialog />
            <Slot />
        </div>

    );
}
