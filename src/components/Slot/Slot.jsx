import React, {createRef, useState} from 'react';

import {getRandomNumber} from "./getRandomNumber";

import './Slot.scss'
const slotValues = [1,2,3,4,5,6,7,8,9]
const SlotMachine = () => {
    const [result, setResult] = useState([0, 0, 0])
    const [rolling, setRolling] = useState(false)
    const slotRef = [createRef(), createRef(), createRef()]
    const spinSlot = () =>{
        slotRef.forEach((slot, i) => {
            const newNumber = triggerSlotRotation(slot.current);
            const newResult = [...result]
            newResult[i] = newNumber
            setResult(newResult)
        });
    }
    const debugSlot = () =>{
            slotRef.forEach((slot, i) => {
                const newNumber = triggerSlotRotation(slot.current);
                const newResult = [...result]
                newResult[i] = newNumber
                setResult(newResult)
        }
    }
    const triggerSlotRotation = (ref) => {
        function setTop(top) {
            ref.style.top = `${top}px`;
        }
        const options = ref.children;
        const randomIdx = getRandomNumber(0,8);
        const chosenOption = options[randomIdx];
        setTop(-chosenOption.offsetTop + 2);
        return randomIdx +1;
    };

    const debuggingSlotRotation = (ref) => {
        function setTop(top) {
            ref.style.top = `${top}px`;
        }
        const options = ref.children;
        const chosenOption = options[6];
        setTop(-chosenOption.offsetTop + 2);
        return 7;
    };

    const debugging = () =>{
       setResult([7,7,7])
    }

    const close = () =>{
        setResult([0,0,0])
    }
        const slotElements = result.map((item, idx)=>
            <div className="slot-result-item">
            <section className='slot-result-item-section'>
                <div className="slot-result-item-container" ref={slotRef[idx]}>
                    {slotValues.map((number, i) => (
                        <div key={i}>
                            <span>{number}</span>
                        </div>
                    ))}
                </div>
            </section>
        </div>)

    return (
        <div className="slot-machine">
            <div className="slot-result">
                {slotElements}
            </div>
            <div className="slot-machine-actions">
                <button className='slot-machine-action' onClick={spinSlot}>start</button>
                <button className='slot-machine-action' onClick={debugging}>debugging</button>
                <button className='slot-machine-action' onClick={close}>close</button>
            </div>
        </div>
    );
};

export default SlotMachine;
