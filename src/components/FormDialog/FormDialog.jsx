import React, {useState} from 'react';
import {Button, DialogContentText} from '@material-ui/core';

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";

export default function FormDialog() {
    const [open, setOpen] = useState(false)

    const handleClickOpen = () =>{
        setOpen(true)
    }

    const handleClose = () =>{
        setOpen(false)
    }

    return (
        <div>
            <Button variant="outlined" color="secondary" onClick={handleClickOpen}>OR||or</Button>
            <Dialog open={open} onClose={handleClose} area-labelledby = 'form-dialog-title'>
                <DialogTitle id='form-dialog-title'>Log in</DialogTitle>
                <DialogContent>
                    <DialogContentText>Log in to registration</DialogContentText>
                    <TextField
                        autoFocus
                        margin='dense'
                        id='name'
                        label='Name'
                        type="Name"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin='dense'
                        id='email'
                        label='Email adress'
                        type="email"
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin='dense'
                        id='pass'
                        label='Password'
                        type="password"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color='primary'>Cancel</Button>
                    <Button onClick={handleClose} color='primary'>Log in</Button>
                </DialogActions>
            </Dialog>
        </div>

    );
}
