import React from 'react';

import './GamePage.scss'

import Header from "../Header";
import Main from "../Main";
import Frooter from "../Footer";

const GamePage = () => {
    return (
        <div>
            <Header />
            <Main />
            <Frooter />
        </div>
    );
};

export default GamePage;
