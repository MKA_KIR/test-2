import {LOGIN} from "./constants";
export const createLogin = (payload) =>{
    const action = {
        type: LOGIN,
        payload
    }
    return action
}
