import {initialState} from "./initialState";

export const reducer = (state = initialState, action)=> {
    switch(action.type) {
        case "LOGIN":
            return {...state, profileData: action.payload};
        default:
            return state;
    }
};
